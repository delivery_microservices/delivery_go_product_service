package grpc

import (
	"gitlab.com/delivery_microservices/delivery_go_product_service/config"
	"gitlab.com/delivery_microservices/delivery_go_product_service/genproto/product_service"
	"gitlab.com/delivery_microservices/delivery_go_product_service/grpc/client"
	"gitlab.com/delivery_microservices/delivery_go_product_service/grpc/service"
	"gitlab.com/delivery_microservices/delivery_go_product_service/pkg/logger"
	"gitlab.com/delivery_microservices/delivery_go_product_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	product_service.RegisterProductServiceServer(grpcServer, service.NewProductService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)

	return
}
