CREATE TYPE "active" AS ENUM (
  'yes',
  'no'
);

CREATE TYPE "product_type" AS ENUM (
  'modifier',
  'product'
);

CREATE TABLE "categories" (
  "id" serial PRIMARY KEY,
  "title" varchar(255) NOT NULL,
  "image" varchar(255) NOT NULL,
  "active" active DEFAULT 'yes',
  "parent_id" int,
  "order_number" serial,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp,
  "deleted_at" timestamp
);

CREATE TABLE "products" (
  "id" serial PRIMARY KEY,
  "title" varchar(255) NOT NULL,
  "description" text NOT NULL,
  "photo" varchar(255) NOT NULL,
  "order_number" serial,
  "active" active DEFAULT 'yes',
  "type" product_type NOT NULL,
  "price" numeric NOT NULL,
  "category_id" int NOT NULL,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp,
  "deleted_at" timestamp
);

ALTER TABLE "categories" ADD FOREIGN KEY ("parent_id") REFERENCES "categories" ("id");

ALTER TABLE "products" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("id");
